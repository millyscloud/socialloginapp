package com.socialogin.controllers;

import com.socialogin.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @GetMapping({ "/", "login"})
    private ModelAndView goToLogin(@AuthenticationPrincipal UserDetails userDetails,
                                   @RequestParam(value = "error", required = false) String error){

        var mav = new ModelAndView();
        var user = new User();

        if (error != null) {
            mav.addObject("error", "Invalid email and/or password!");
        }

        mav.addObject("newUser", user);
        mav.setViewName("login");
        return mav;

    }

    @GetMapping("/socialLogin/{registrationId}")
    public ModelAndView socialLogin(@PathVariable("registrationId") String registrationId) {

        var mav = new ModelAndView();
        mav.setViewName("redirect:" + "/oauth2/authorize/" + registrationId);
        return mav;
    }
}
