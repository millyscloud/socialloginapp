package com.socialogin.services.user;

import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import javax.management.Query;
import java.util.Map;

@Service
public class CustomOidcUserService extends OidcUserService {

    @Override
    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
        OidcUser oidcUser = super.loadUser(userRequest);
        Map<String,Object> attributes = oidcUser.getAttributes();
//        System.out.println((String) attributes.get("email"));
//        System.out.println((String) attributes.get("sub"));
//        System.out.println((String) attributes.get("picture"));
//        System.out.println((String) attributes.get("name"));
        attributes.keySet().forEach(k -> System.out.println(k.toString()));
        return oidcUser;
    }
}
