package com.socialogin.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.socialogin.model.enums.RoleEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "roles")
@Getter @Setter @NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long roleId;

    @Column(name = "name")
    @Enumerated
    private RoleEnum name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<User> users;


    public Role(RoleEnum name) {
        this.name = name;
    }

    public Role(RoleEnum name, List<User> users) {
        this.name = name;
        this.users = users;
    }
}
