package com.socialogin.model.enums;

public enum RoleEnum {

    ROLE_USER("User"),
    ROLE_ADMIN("Admin");

    private String displayName;

    public String getDisplayName() {
        return this.displayName;
    }

    RoleEnum(String displayName) {
        this.displayName = displayName;
    }

    public static RoleEnum getByName(String name) {
        for (RoleEnum roleEnum : RoleEnum.values()) {
            if (roleEnum.displayName.equalsIgnoreCase(name)) {
                return roleEnum;
            }
        }
        return null;
    }
}
